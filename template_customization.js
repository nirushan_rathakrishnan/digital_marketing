function enableEditableImages() {
    addEditImageModal();
    addEditImageModalEvents();
    addImageChangeBtn();
    let allImages = $("img").not(".modal-desktop-img, .modal-mobile-img");
    $.each(allImages, function (index, img) {
        $(img).parent().hover(function () {
            $(this).append($(".middle-btn-container"));
            $(".middle-btn-container").css({ "opacity": "1" });
            $(img).css({ "opacity": ".7" });
            $('.change-img-btn').data("img", $(img));
        }, function () {
            $(".middle-btn-container").css({ "opacity": "0" });
            $(img).css({ "opacity": "1" });
        });

        $('.change-img-btn').on('click', function (e) {
            $("#imageModal").css({ "display": "block" });
            let img = $(this).data("img");
            let mobileUrl = img.attr("mobileurl") !== undefined ? img.attr("mobileurl") : img.attr("src");
            let desktopUrl = img.attr("desktopurl") !== undefined ? img.attr("desktopurl") : img.attr("src");
            $(".modal-desktop-img").attr("src", desktopUrl);
            $(".modal-mobile-img").attr("src", mobileUrl);
            $(".modal-desktop-img").data("act-img", img);
            $(".modal-mobile-img").data("act-img", img);
        });
    });
}

function addEditImageModal() {
    $("body").append('<div id="imageModal" class="modal"></div>');
    $(".modal").append('<div class="modal-content"></div>');
    $(".modal-content").append('<span id="modalClose" class="close">&times;</span>');

    $(".modal-content").append('<h4>Desktop Image</h4>');
    $(".modal-content").append('<img class="modal-desktop-img">');
    $(".modal-content").append('<form method="post" class="upload-desktop-img-form" enctype="multipart/form-data"></form><br>');
    $(".upload-desktop-img-form").append('<br><input type="file" name="image_file" id="image_file"><br>');
    $(".upload-desktop-img-form").append('<input type="submit" name="upload" id="upload" value="Upload" class="btn btn-info">');

    $(".modal-content").append('<h4>Mobile Image</h4>');
    $(".modal-content").append('<img class="modal-mobile-img">');
    $(".modal-content").append('<form method="post" class="upload-mobile-img-form" enctype="multipart/form-data"></form>');
    $(".upload-mobile-img-form").append('<br><input type="file" name="image_file" id="image_file"><br>');
    $(".upload-mobile-img-form").append('<input type="submit" name="upload" id="upload" value="Upload" class="btn btn-info">');
    
    $(".modal").css({
        "display": "none",
        "position": "fixed",
        "z-index": "7",
        "left": "0",
        "top": "0",
        "width": "100%",
        "height": "100%",
        "overflow": "auto",
        "background-color": "rgb(0,0,0)",
        "background-color": "rgba(0,0,0,0.4)"
    });
    $(".modal-content").css({
        "background-color": "#fefefe",
        "margin": "15% auto",
        "padding": "20px",
        "border": "1px solid #888",
        "width": "80%"
    });
    $(".close").css({
        "color": "#aaa",
        "float": "right",
        "font-size": "28px",
        "font-weight": "bold",
        "text-decoration": "none",
        "cursor": "pointer"
    });
    $(".close").hover(function () {
        $(".close").css({
            "color": "black"
        });
    }, function () {
        $(".close").css({
            "color": "#aaa"
        });
    });
}

function addImageChangeBtn() {
    $("body").append('<div class="middle-btn-container" id="imageChangeBtnContainer" style="position: absolute; top: 50%; left: 50%; opacity: 0"><button class="change-img-btn">Change</button></div>');
}

function addEditImageModalEvents() {
    $('.upload-desktop-img-form').on('submit', function (e) {
        e.preventDefault();
        uploadModalImage(this, function (url) {
            let img = $(".modal-desktop-img").data("act-img");
            if (!isMobile()) {
                img.attr("src", url);
            }
            $(".modal-desktop-img").attr("src", url);
            img.attr("desktopurl", url);
        }, function(err) {
            $("#desktop-img-err").text(err);
        });
    });
    
    $('.upload-mobile-img-form').on('submit', function (e) {
        e.preventDefault();
        uploadModalImage(this, function (url) {
            let img = $(".modal-mobile-img").data("act-img");
            if (isMobile()) {
                img.attr("src", url);
            }
            $(".modal-mobile-img").attr("src", url);
            img.attr("mobileurl", url);
        }, function(err) {
            $("#mobile-img-err").text(err);
        });
    });

    $(".close").on('click', function (e) {
        $("#imageModal").css({ "display": "none" });
    });
}

function uploadModalImage(form, successCallback, errorCallback) {
    let pathname = window.location.pathname;
    let pathnameElements = pathname.split('/');
    let templateName = pathnameElements[pathnameElements.length - 2];
    let formdata = new FormData(form);
    formdata.append('templateName', templateName);
    $.ajax({
        url: "http://localhost:8080/digitalmarketing/s3upload/",
        method: "POST",
        data: formdata,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
        },
        success: function (result) {
            result = JSON.parse(result);
            console.log("image saved.", result);
            if (result.status) {
                successCallback(result.url);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error..');
            console.log(errorThrown);
            console.log(textStatus);
        },
        complete: function () {
        }
    });
}

function isMobile() {
    var isMobile = false; //initiate as false
    // device detection
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
        isMobile = true;
    } else if ($(window).width() < 600) {
        isMobile = true;
    }
    console.log("Device type is Mobile? " + isMobile);
    return isMobile;
}

let custom_fields = [
    { name: 'google_conversion_pixel', label: ' Google Conversion Pixel ID', type: 'pixel' },
    { name: 'taboola_conversion_pixel', label: 'Taboola Conversion Pixel ID', type: 'pixel' },
    { name: 'mobile_calling', label: 'Mobile Phone Number', type: 'calling' },
    { name: 'desktop_calling', label: 'Desktop Phone Number', type: 'calling' },
    { name: 'webhook_fb', label: 'Webhook For facebook', type: 'webhook' },
    { name: 'webhook_taboola', label: 'Webhook for taboola', type: 'webhook' },
    { name: 'webhook_linkedin', label: 'Webhook for linkedin', type: 'webhook' },
    { name: 'webhook_times', label: 'Webhook for times', type: 'webhook' },
    { name: 'webhook_google', label: 'Webhook for google', type: 'webhook' },
    { name: 'title', label: 'Page Title', type: 'title' },
    { name: 'description', label: 'Page description', type: 'meta' },
    { name: 'keywords', label: 'Page keywords', type: 'meta' },
];
var url = window.location.href;
$('.logo_header').show();

if (url.indexOf('editable_') == -1) {
    $('.development_div').hide();
    $('.intro_heading, .intro_para, .feed-title, .feed-para, .agile-head, .title-text,.desktop, .title , .description, .card-title, .list-group-item-text , .sidenav , .tour-heading ,.tour-para ,.enquire_form ,.navbar-brand, .post-details, .editable, .about_builder, address').prop('contentEditable', 'false');
} else {
    $('.development_div').show();
    $('.intro_heading, .intro_para, .feed-title, .feed-para, .agile-head, .title-text,.desktop, .title , .description, .card-title, .list-group-item-text , .sidenav , .tour-heading ,.tour-para ,.enquire_form ,.navbar-brand, .post-details, .editable, .about_builder, address').prop('contentEditable', 'true');
    create_custom_input_fields();
    enableEditableImages();
}

function get_var_declaration(field_class, variable_name) {
    let webhook_url_val = $('.' + field_class).val();
    $('.' + field_class + '_hidden').val(webhook_url_val)
    if (webhook_url_val.length > 0) {
        return "var " + variable_name + " = \"" + webhook_url_val + "\";\n";
    } else {
        return "";
    }
}

function show_input_values(hidden_field_name, field_name) {
    let hidden_field_val = $('.' + hidden_field_name).val();
    $('.' + field_name).val(hidden_field_val);
}

//################## trying custom function start ###############

// { name : 'project_name', label : 'Project Name', type : 'meta' },
function create_custom_input_fields() {
    custom_fields.forEach(function (custom_field) {
        let hidden_custom_field = custom_field.name + '_hidden';
        let custom_field_div = custom_field.name + '_div';
        // let checking_custom_div = $('.development_div').find(custom_field_div).length;
        if ($('.development_div').find('.' + custom_field_div).length == 0) {
            let label_html_field = '<div class="' + custom_field_div + '"><label class="add_section">' + custom_field.label + '</label><input type="text" class="' + custom_field.name + '" /> <input type="hidden" class="' + hidden_custom_field + '" /></div>';
            $('.development_div').prepend(label_html_field);
        }
        if (custom_field.type == 'calling') {
        }
        show_input_values(hidden_custom_field, custom_field.name);
    });
}


function get_var_declarations() {
    let var_declaration_scripts = "";
    custom_fields.forEach(function (custom_field) {
        if (custom_field.type == 'meta') {
            var custom_field_value = $('.' + custom_field.name).val();
            $('meta[name=' + custom_field.name + ']').attr('content', custom_field_value);
            $(custom_field.name).attr('content', custom_field_value);
            $(custom_field.name + '_hidden').attr('value', custom_field_value);
        }
        if (custom_field.type == 'title') {
            var custom_field_value = $('.' + custom_field.name).val();
            $('.' + custom_field.name + '_hidden_header').text(custom_field_value);
        }


        // else {
        // 	var_declaration_scripts += get_var_declaration(custom_field.name, custom_field.name);
        // }
        var_declaration_scripts += get_var_declaration(custom_field.name, custom_field.name);
    });
    return var_declaration_scripts;
}


function add_calling_buttons() {
    let calling_fields = ['mobile_calling', 'desktop_calling'];
    calling_fields.forEach(function (field) {
        if ($('.' + field + '_hidden').val()) {
            let call_button = '';
            let phone_number = $('.' + field + '_hidden').val()
            let device_class = (field == 'mobile_calling') ? 'mobile_call_button' : 'desktop_call_button';
            if (field == 'mobile_calling') {
                call_button = '<a href="tel:' + phone_number + '" class=" call_float_button ' + device_class + ' ' + field + '_button "><i class="fa fa-phone my_float_div"></i></a>';
            } else {
                call_button = '<a data-toggle="modal" data-target="#click_to_call_Modal" class=" call_float_button ' + device_class + ' ' + field + '_button"><i class="fa fa-phone my_float_div"></i></a>'
            }
            $('.calling_section').prepend(call_button);
        }
    });
}

function clean_up_page_before_saving() {
    // Remove calling button if input value is empty
    let cleanup_fields = ['mobile_calling', 'desktop_calling'];
    cleanup_fields.forEach(function (field) {
        if ($('.' + field + '_hidden').val() == '') {
            $('.' + field + '_button').remove();
        }
    });

    // clean all src from the page
    document.removeEventListener("scroll", lazyLoadHandler);
    window.removeEventListener("resize", lazyLoadHandler);
    window.removeEventListener("orientationChange", lazyLoadHandler);

    // remove image editing modal
    $("#imageModal").remove();
    $("#imageChangeBtnContainer").remove();

    let allImages = $("img");
    $.each(allImages, function (index, img) {
        img = $(img);
        let mobileUrl = img.attr("mobileurl") !== undefined ? img.attr("mobileurl") : img.attr("src");
        let desktopUrl = img.attr("desktopurl") !== undefined ? img.attr("desktopurl") : img.attr("src");
        img.attr("desktopurl", desktopUrl);
        img.attr("mobileurl", mobileUrl);
        img.attr("src", "loading.gif");
        img.addClass("loading-img");
        img.removeClass("lazy-loaded");
    });

    // TODO: Clean up all scripts (facebook, taboola etc)
}

function cleanAutoAddedScriptAndSave(saveCallback) {
    $.ajax({
        url: window.location.href,
        type: 'GET',
        success: function (originContent) {
            $('script').each(function (index, currentScript) {
                let exist = scriptExists(originContent, currentScript);
                if ($(currentScript).attr("id") !== "development_script" && !exist) {
                    // console.info("script to be removed", currentScript);
                    $(currentScript).remove();
                }
            });
            saveCallback();
        }
    });
}

function scriptExists(originContent, currentScript) {
    let isFound = false;
    $(originContent).filter('script').each(function (index2, originScript) {
        if (!isFound) {
            isFound = currentScript.outerHTML === originScript.outerHTML;
        }
    });
    return isFound;
}

//################## trying custom function end ###############



$('#save_landing_page_button').on("click", function (e) {
    e.preventDefault();
    var var_declaration_scripts = "";
    var_declaration_scripts += get_var_declarations();

    if ($('#development_script').length == 0) {
        $('body').append('<script id="development_script"></script>');
    }

    $('#development_script').text(var_declaration_scripts);

    // TODO: Clean up page before saving
    clean_up_page_before_saving();
    add_calling_buttons();

    cleanAutoAddedScriptAndSave(function () {
        var html_data = { "head_html": $('head').html(), "body_html": $('body').html() };

        $.ajax({
            url: "https://propstory.com/optima/Zapier_data/save_landing_page",
            type: 'POST',
            data: html_data,
            dataType: "text",
            success: function (result) {
                console.log(result);
                console.log('Success!Landing Page Saved Successfully');
                // $('.lp_success_msg').show();
                alert('Landing page save succesfully');

                location.reload();
            }
        });
    });

});

function upload_image(form) {
    // console.log(form);
    // console.log(new FormData(form));
    var field = $(form).find('.field_name').val();
    console.log(field);
    $.ajax({
        url: "https://propstory.com/optima/Zapier_data/save_uploaded_builder_logo",
        method: "POST",
        data: new FormData(form),
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
            $('.overlay_with_high_zindex').show();
        },
        success: function (data) {
            console.log('data');
            console.log(data);
            $('.' + field).attr("src", "");
            $('.' + field).attr("data-src", "");
            if (field == 'fav_icon') {
                $('.' + field).attr("href", data);
            } else {
                $('.' + field).attr("data-src", data);
            }
            if ($(form).find('.' + field + '-img').length > 0) {
                $('.' + field + '-img').attr("src", data);
            } else {
                $('.' + field + '_mobile-img').attr("src", data);
            }
            $('.overlay_with_high_zindex').hide();
            //show thumbnail after uploading
            // $('#uploaded_image').html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error..');
            console.log(errorThrown);
            console.log(textStatus);
        },
        complete: function () {
            $('.overlay_with_high_zindex').hide();
            // console.log('abc');
        }
    });
}

$('.lp_success_msg').hide();

$('#upload_builder_logo_form').on('submit', function (e) {
    e.preventDefault();
    // console.log(this);
    upload_image(this)
});

$('#upload_project_logo_form').on('submit', function (e) {
    e.preventDefault();
    upload_image(this)
});

$('#upload_favicon_form').on('submit', function (e) {
    e.preventDefault();
    uploadModalImage(this, function (url) {
        $("#favicon-upload-img").attr("src", url);
        $('link[rel="icon"]').attr('href',url);
    }, function(err) {
        console.log("favicon upload error", err);
    });
});

//var cloneCount = 1;
$('.add_new_section').on('click', function () {
    //var new_section = $('.section-03').html();

    var new_section = $('#amenities').clone().attr('id', '').insertAfter('#amenities');
    clone_section = $(new_section).html();

    //$(new_section).attr('class', 'newClass');
    // .attr('id', 'id'+ cloneCount++)
    //$('.section-03').removeAttr("id");
    //$(new_section).css('border', '3px solid red');

    //$('.new_section-03').click(function(){ 
    //window.close();
    //});

    alert('Section Added to Your Page </br> 3rd section cloned ');
});