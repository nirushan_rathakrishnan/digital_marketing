// Lazy loading scripts
var lazyLoadHandler = undefined;

$(document).ready(function () {
    enableLazyLoadingImages();
});

function enableLazyLoadingImages() {
    var lazyloadThrottleTimeout;

    function lazyload() {
        var lazyloadImages = $("img").not(".lazy-loaded");
        if (lazyloadThrottleTimeout) {
            clearTimeout(lazyloadThrottleTimeout);
        }

        lazyloadThrottleTimeout = setTimeout(function () {
            var scrollTop = window.pageYOffset;
            $.each(lazyloadImages, function (index, img) {
                img = $(img);
                if (img.offset().top < (window.innerHeight + scrollTop)) {
                    let mobileUrl = img.attr("mobileurl") !== undefined ? img.attr("mobileurl") : img.attr("src");
                    let desktopUrl = img.attr("desktopurl") !== undefined ? img.attr("desktopurl") : img.attr("src");
                    let src = isMobile() ? mobileUrl : desktopUrl;
                    img.attr("src", src);
                    // img.removeClass("loading-img");
                    img.addClass("lazy-loaded");
                }
            });
        }, 20);
    }
    lazyLoadHandler = lazyload;
    document.addEventListener("scroll", lazyLoadHandler);
    window.addEventListener("resize", lazyLoadHandler);
    window.addEventListener("orientationChange", lazyLoadHandler);
    lazyload();
}