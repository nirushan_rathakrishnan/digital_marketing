<?php
    require 'vendor/autoload.php';
    
    use Aws\S3\S3Client;
    use Aws\S3\Exception\S3Exception;

    $result = array();
    $result['status'] = true;
    $templateName = $_POST['templateName'];
    try {
        if (!file_exists('/tmp')) {
            mkdir('/tmp');
        }

        $fileName = $_FILES['image_file']['name'];
        $sourcePath = $_FILES['image_file']['tmp_name'];
        $targetPath = "/tmp//".$fileName;
        move_uploaded_file($sourcePath, $targetPath);
        
        $bucketName = 'pslpuploads';
        $IAM_KEY = 'AKIAXR4ZBUGQCYNBEOBU';
        $IAM_SECRET = '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';

        // Set Amazon S3 Credentials
        $s3 = S3Client::factory(
            array(
                'credentials' => array(
                    'key' => $IAM_KEY,
                    'secret' => $IAM_SECRET
                ),
                'version' => 'latest',
                'region'  => 'ap-south-1'
            )
        );

	    $keyName = $templateName . '/' . uniqid() . basename($fileName);

        // Put on S3
        $s3Result = $s3->putObject(
            array(
                'Bucket'=>$bucketName,
                'Key' =>  $keyName,
                'SourceFile' => $targetPath,
                'StorageClass' => 'REDUCED_REDUNDANCY'
            )
        );
        $result['url'] = $s3Result['ObjectURL'];
    } catch (S3Exception $e) {
        $result['status'] = false;
        $result['message'] = $e->getMessage();
    } catch (Exception $e) {
        $result['status'] = false;
        $result['message'] = $e->getMessage();
    }
    echo json_encode($result);
