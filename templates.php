<?php
    // Constants variables
    define("CAMPAIGN_PATH", "campaign/");
    define("TEMPLATE_PATH", "templates/");
?>

<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 300px;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.preview-frame {
    width: 100%;
    height: 100%;
}

.preview-container {
    width: 100%;
    height: 75%;
    position: fixed;
    bottom: 0;
    display: none;
}

.msg-box {
    border: 1px solid #cecece;
    color: green;
    font-size: 1.5em;
    padding: 1em;
    display: none;
}
</style>

<div class="msg-box">
</div>
<?php

function getCurrentUrl()
{
    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
        $link = "https";
    } else {
        $link = "http";
    }
    $link .= "://";
    $link .= $_SERVER['HTTP_HOST'];
    return str_replace("templates.php", "", $link . $_SERVER['REQUEST_URI']);
}

function listDownTemplates()
{
    echo '<h2>Land Page Templates</h2>';
    echo '<table>';
    echo '<tr>';
    echo '<th>Project Name</th>';
    echo '<th>Preview</th>';
    echo '<th>Open</th>';
    echo '<th>Action</th>';
    echo '</tr>';
    $alltempDir = scandir(TEMPLATE_PATH);
    foreach ($alltempDir as $allTempValue) {
        $tempFolderPath = TEMPLATE_PATH . DIRECTORY_SEPARATOR . $allTempValue;
        if (!in_array($allTempValue, array(".","..")) && is_dir($tempFolderPath)) {
            echo '<tr>';
            $tempDir = scandir($tempFolderPath);
            foreach ($tempDir as $tempValue) {
                if (!in_array($tempValue, array(".","..")) && strpos($tempValue, 'editable_') !== false) {
                    $url = getCurrentUrl() . TEMPLATE_PATH . '/' . $allTempValue . '/' . $tempValue;
                    echo '<td>'.$allTempValue.'</td>';
                    echo '<td><a href="javascript:showPreview(\''.$url.'\')">Preview</a></td>';
                    echo '<td><a href="'.$url.'" target="_blank">Open</a></td>';
                    echo '<td><a href="javascript:createTemplate(\''.$allTempValue.'\',\''.$tempValue.'\')">Clone</a></td>';
                }
            }
            echo '</tr>';
        }
    }
    echo '</table>';
}

listDownTemplates();
?>

<div class="preview-container">
    <button onclick="closePreview()">Close Preview</button>
    <br>
    <iframe id="previewFrame" class="preview-frame"></iframe>
</div>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <div>
        <span class="close">&times;</span>
    </div><br>
    <h4 id="new-template-header"></h4>
    <div>
        <input id="new-project-name" placeholder="Enter new project name"/><br><br>
        <button id="create-new-btn">Create</button><br><br>
        <p id="error-msg" style="color: red"></p>
    </div>
  </div>

</div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

let selectedTemplateName = "";
let selectedHtmlFileName = "";
$('#create-new-btn').on('click', function() {
    
    let baseUrl = <?php echo '"'. getCurrentUrl() . '";';?>
    let campaignPath = <?php echo '"'. CAMPAIGN_PATH . '";';?>
    let newProjectName = $('#new-project-name').val();
    newProjectName = newProjectName.replace(/\s/g, '');
        $.ajax({
            url: baseUrl + "/create-project.php",
            type: 'POST',
            data: {
                fromTemplate: selectedTemplateName,
                newTemplateName: newProjectName
            },
            dataType: "text",
            success: function (result) {
                result = JSON.parse(result);
                if (result.status) {
                    modal.style.display = "none";
                    let url = baseUrl + campaignPath + newProjectName + '/' + selectedHtmlFileName;
                    $('#new-project-name').val("");
                    $('.msg-box').css({ "display" : "block" });
                    $('.msg-box').html(result.message + " <a href='" + url + "' target='_blank'>Click here to open the created project</a>");
                } else {
                    $('#error-msg').text(result.message);
                }
            }
        });
});

function createTemplate(templateName, htmlFileName) {
    selectedTemplateName = templateName;
    selectedHtmlFileName = htmlFileName;
    $('#new-template-header').text("Create new template from " + templateName);
    modal.style.display = "block";
}

function showPreview(url) {
    console.log("open url", url);
    $(".preview-container").css({ "display": "block" });
    $("#previewFrame").attr("src", url);
}

function closePreview() {
    $(".preview-container").css({ "display": "none" });
}
</script>