<?php
    function save_landing_page()
    {
        $head_html =  $this->input->post('head_html');
        $body_html = $this->input->post('body_html') ;
    
        $html_content = "<html> <head> " . $head_html . "</head> <body> ". $body_html . "</body></html>";
        // $html = "<html>" . $this->input->post('head') . "</html>";
        // $html = "<body>" . $this->input->post('body') . "</body>";
        $landing_page_url = $_SERVER['HTTP_REFERER'];
        $server_root = $_SERVER['DOCUMENT_ROOT'];
        $landing_page_path = str_replace($_SERVER['HTTP_ORIGIN'], $server_root, $landing_page_url);
        // $landing_page_path = str_replace($_SERVER['HTTP_ORIGIN'],  $server_root, $landing_page_url);
        $date = date("Y-m-d-h-i-s");
        $version_path = $landing_page_path. "-" .$date. ".html";
        $success = $this->save_file($landing_page_path, $html_content);
        $this->save_file($version_path, $html_content);
        if ($success) {
            echo "$landing_page_path saved successfully.";
            echo $version_path;
        } else {
            echo "$landing_page_path failed to save.";
            echo $version_path;
        }
    }

  function save_file($file_name, $content)
  {
      $file = fopen($file_name, "w");
      if (false == $file) {
          return false;
      }
      fwrite($file, $content);
      fclose($file);
      return true;
  }
