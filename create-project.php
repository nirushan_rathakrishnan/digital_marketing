<?php
    // Constants variables
    define("CAMPAIGN_PATH", "campaign/");
    define("TEMPLATE_PATH", "templates/");
    
    $sourceProject = $_POST['fromTemplate'];
    $newProject = $_POST['newTemplateName'];

    function copyFiles($source, $dest) {
        $cdir = scandir($source);
        foreach ($cdir as $value)
        {
           if (!in_array($value,array(".","..")))
           {
              if (is_dir($source . DIRECTORY_SEPARATOR . $value))
              {
                    // create dir and iterate files
                    if (!mkdir($dest . DIRECTORY_SEPARATOR . $value, 0700, true)) {
                        throw new Exception('Failed to create folder - '. $dest . DIRECTORY_SEPARATOR . $value);
                    }
                    $result[$value] = copyFiles($source . DIRECTORY_SEPARATOR . $value, $dest . DIRECTORY_SEPARATOR . $value);
              }
              else
              {
                    //copy file
                    if (!copy($source . DIRECTORY_SEPARATOR . $value, $dest . DIRECTORY_SEPARATOR . $value)) {
                        throw new Exception('Failed to copy file - '. $dest . DIRECTORY_SEPARATOR . $value);
                    }
                    $result[] = $value;
              }
           }
        }
     }

    $newProjectPath = CAMPAIGN_PATH.$newProject;
    if (file_exists($newProjectPath)) {
        echo json_encode(array('status' => false, 'message' => 'Project name '. $newProject .' already exists.'));
        exit;
    }

    try {
        if (!file_exists(CAMPAIGN_PATH)) {
            if (!mkdir(CAMPAIGN_PATH, 0700, true)) {
                throw new Exception('Failed to create folder - ' . $newProject. '. Tried to create path: ' . $campaignPath);
            }
        }
        if (!mkdir($newProjectPath, 0700, true)) {
            throw new Exception('Failed to create landing page folder - ' . $newProject . '. Tried to create path: ' . $newProjectPath);
        }
        copyFiles(TEMPLATE_PATH.$sourceProject, $newProjectPath);
        echo json_encode(array('status' => true, 'message' => 'Project ' .$newProject. ' created successfully.'));
    } 
    catch (Exception $e) {
        echo json_encode(array('status' => false, 'message' => $e->getMessage()));
    }
?>